# HA Proxy Setup & Scripts

This repo will have scripts to install an HA Proxy Loadbalancer.

Please follow the instructions to get setup.


### Pre REquisits

To follow this guide you'll need:

- AWS access
- 3 Ubuntu machines with public IP
- Key pairs


### Intallation

This is going to be a step by step to first install 2 ngix servers and then Ha Proxy.

First git clone this repo

```bash
$ git clone git@bitbucket.org:Filipe-p/ha_proxy_setup.git
```

Then you can continue the setup.

#### **Nginx servers**

You'll need two ngix servers so the Ha proxy can load balance into.

##### Step 1 - Install ngnix
For each machnie run this command

```bash
$ ./ubuntu.sh <ip_of_first_ubuntu>

$ ./ubuntu.sh <ip_of_second_ubuntu>
```

##### Step 2 Uniqly identify each nginx machine

Only do this step for testing only, both machines should be the same.
For test add tags to each index.html machine

```bash 
SSh into the machine
Edit /etc/www/html/index

```


#### **HA proxy**

Now will do the step by step for the Ha proxy.

Method 1:

- 1) Send config file to remote machine
- 2) run bash script that install Ha_proxy & moves the config & restarts haproxy 


Method 2:

- 2) run bash script that install Ha_proxy & creates config file

method 3:

- 2) run bash script that install Ha_proxy & creates config file and used `sed`

